## Seja bem vindo ao módulo de configuração do Docker

O autor desse módulo é o [Matheus Politano](https://www.matheuspolitano.com/)
![Docker](https://miro.medium.com/max/2480/1*9hGvYE5jegHm1r_97gH-jQ.png)



## O que é o docker?

Docker é um encapsulamento de um sistema unix isolado


## O que é uma imagem?

Inclui elementos necessários para executar uma aplicação como um container 

- Código
- Arquivos de configuração
- Variáveis de ambiente 
- Bibliotecas 
- Tempo de execução

## Como executar uma imagem?

``` console
	docker run -it <name_image> /bin/bash	
```

Nesse caso ele irá abrir dentro do terminal, para sair e finalizar o container digit "exit"

## Como criar uma imagem?

Existem duas maneiras de criar uma imagem a primeira é utilizando o dockerhub, que é o repositório
do docker a segunda é através do Dockerfile

### Atrvés do Dockerhub
	
``` console
	docker run -it ubuntu:lasted /bin/bash
	apt-get -yqq update
	apt-get -y install apache2

```
Agora saia do container com "exit", e pegue o id do container com o comando "docker ps -q"

``` console
	docker commit -m "message" -a "message-autor" <id> <nameimage>

```

Para executar agr basta 

``` console
`	docker run -it <nameimage> /bin/bash

```





